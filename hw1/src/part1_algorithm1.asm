Main:
	mov R0, #4h		;input
	CALL Fact
	ljmp finish

;this algorithm has this pseudocode
; int fact(int n){
	; int i = 2, prod =1;
	; while (i <= n){
		; prod = prod * i;
		; i = i + 1;
	; }
	; return prod;
; }

Fact:
	mov R1, #2h		;i
	mov R2, #1h		;prod
CheckLoop:
	;jump if i>input
	mov A, R0
	mov B, R1
	cjne A, B, CheckGreater
	SJMP Exec
CheckGreater:
	JC Return
Exec:
	;prod = prod * i
	mov A, R2
	mov B, R1
	mul AB
	mov R2, A
	;i = i + 1
	mov A, R1
	add A, #1h
	mov R1, A
	SJMP CheckLoop
Return:
	;A = prod
	mov A, R2
	RET
finish:
	end