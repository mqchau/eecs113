mov R0, #5		;input
LCALL Fact
ljmp Finish

;this algorith is simple
; int fact(int n){
	; return FactTable[n];
; }
Fact:
mov A, R0				;move input to A, that's all so the index
mov dptr,#FactTable		;load address of database to dptr
movc a,@a+dptr			;access the data by index + base
ret						;done return to upper level of stack

FactTable:				;factorial table for 1 - 5
	db 1,1,2,6,24,120

Finish:
end
