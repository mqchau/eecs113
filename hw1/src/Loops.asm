Main:
mov A, #10h
;Call ForLoop
;Call InfLoop
;Call WhileLoop
;call DoWhileLoop

end


InfLoop:
;your code here
sjmp InfLoop
RET

ForLoop:; for(int i = A; i > 0; i--){}
sjmp check
code:
; your code starts here
nop; example
nop; example
;your code ends here
DEC A
check:
jnz code ;if A not equal 0 go to code
Ret

WhileLoop: ; while(A == 0){}
jnz exit1
; code starts here
nop
;code ends here
sjmp WhileLoop
exit1:
Ret


DoWhileLoop:; do {} while(A == 0)
; code starts here
nop
;code ends here
jnz exit2
sjmp WhileLoop
exit2:
Ret
