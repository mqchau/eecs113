	;input for function
mov R0, #40h		;destination
mov R1, #64			;base
mov R2, #0FFh		;input number
call WriteBase
ljmp  Final

WriteBase:
	mov A, R2
ContinueWriteBase:
	mov B, R1
	div ab
	mov @R0, B
	inc R0	
	jnz ContinueWriteBase
	mov @R0, #80h
	ret

Final:
	end
